//
//  TimerServiceTests.swift
//  TimerAppTests
//
//  Created by Krzysztof Maraszkiewicz on 10/10/2021.
//

import XCTest

class TimerServiceTests: XCTestCase {

    private var serviceUnderTests : TimerService!
    
    override func setUpWithError() throws {
        serviceUnderTests = TimerService()
    }

    func testConvertingSecondsToTimerModelClass() {
        var timerModel = serviceUnderTests.getTimeFromSeconds(secondsToEnd: 3661)
        
        XCTAssertEqual(timerModel, TimeModel(hours: 1, minutes: 1, seconds: 1))
        
        timerModel = serviceUnderTests.getTimeFromSeconds(secondsToEnd: 3600)
        
        XCTAssertEqual(timerModel, TimeModel(hours: 1, minutes: 0, seconds: 0))
        
        timerModel = serviceUnderTests.getTimeFromSeconds(secondsToEnd: 60)
        
        XCTAssertEqual(timerModel, TimeModel(hours: 0, minutes: 1, seconds: 0))
        
        timerModel = serviceUnderTests.getTimeFromSeconds(secondsToEnd: 1)
        
        XCTAssertEqual(timerModel, TimeModel(hours: 0, minutes: 0, seconds: 1))
        
        timerModel = serviceUnderTests.getTimeFromSeconds(secondsToEnd: 61)
        
        XCTAssertEqual(timerModel, TimeModel(hours: 0, minutes: 1, seconds: 1))
    }
    
    func testConvertingTimerModelToTimeInString() {
        var timeInString = serviceUnderTests.getDescriptionTime(timeModel: TimeModel(hours: 1, minutes: 1, seconds: 1))
        XCTAssertEqual(timeInString, "01:01:01")
        
        timeInString = serviceUnderTests.getDescriptionTime(timeModel: TimeModel(hours: 12, minutes: 12, seconds: 12))
        XCTAssertEqual(timeInString, "12:12:12")
        
        timeInString = serviceUnderTests.getDescriptionTime(timeModel: TimeModel(hours: 0, minutes: 0, seconds: 12))
        XCTAssertEqual(timeInString, "00:00:12")
        
    }
    
    func testGetSecondsFromTime() {
        var timeInSeconds = serviceUnderTests.getSecondsFromTime(timerModel: TimeModel(hours: 1, minutes: 1, seconds: 1))
        XCTAssertEqual(timeInSeconds, 3661)
        
        timeInSeconds = serviceUnderTests.getSecondsFromTime(timerModel: TimeModel(hours: 0, minutes: 1, seconds: 1))
        XCTAssertEqual(timeInSeconds, 61)
        
        timeInSeconds = serviceUnderTests.getSecondsFromTime(timerModel: TimeModel(hours: 0, minutes: 0, seconds: 1))
        XCTAssertEqual(timeInSeconds, 1)
    }

}
