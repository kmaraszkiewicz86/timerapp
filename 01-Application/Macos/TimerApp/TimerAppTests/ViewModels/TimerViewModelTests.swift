//
//  TimerViewModelTests.swift
//  TimerAppTests
//
//  Created by Krzysztof Maraszkiewicz on 10/10/2021.
//

import XCTest

class TimerViewModelTests: XCTestCase {

    private var serviceUnderTests : TimerViewModel!
    
    override func setUpWithError() throws {
        serviceUnderTests = TimerViewModel(TimerService())
    }
    
    func testUserClicksStartTimerButton() {
        serviceUnderTests.seconds = 1
        
        serviceUnderTests.timerButtonWork()
        
        XCTAssertEqual(serviceUnderTests.stopStartButtonName, "Stop")
    }
    
    func testUserClickStopTimerButton() {
        serviceUnderTests.stopStartButtonName = "Stop"
        
        serviceUnderTests.timerButtonWork()
        
        XCTAssertEqual(serviceUnderTests.stopStartButtonName, "Start")
    }
}
