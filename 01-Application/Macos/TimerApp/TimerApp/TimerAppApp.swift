//
//  TimerAppApp.swift
//  TimerApp
//
//  Created by Krzysztof Maraszkiewicz on 10/10/2021.
//

import SwiftUI

@main
struct TimerAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(timerViewModel: TimerViewModel(TimerService()))
        }
    }
}
