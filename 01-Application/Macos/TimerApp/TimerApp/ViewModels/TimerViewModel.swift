//
//  TimerViewModel.swift
//  TimerApp
//
//  Created by Krzysztof Maraszkiewicz on 10/10/2021.
//

import Foundation
import SwiftUI

class TimerViewModel : ObservableObject {
    
    var timerService : TimerService
    
    @Published var stopStartButtonName = "Start"
    
    @Published var hours = 0
    
    @Published var minutes = 0
    
    @Published var seconds = 0
    
    @Published var remainingTime = "00:00:00"
    
    private var shouldTimerRun = true
    
    init(_ timerService: TimerService) {
        self.timerService = timerService
    }
    
    func timerButtonWork() {
        if (stopStartButtonName == "Start") {
            start()
        } else {
            stop()
        }
    }
    
    func start() {
        
        var secondsToEnd = timerService.getSecondsFromTime(timerModel: TimeModel(hours: hours, minutes: minutes, seconds: seconds))
        
        if secondsToEnd <= 0 {
            return
        }
        
        shouldTimerRun = true
        stopStartButtonName = "Stop"
        
        DispatchQueue.global().async {
            while (secondsToEnd > 0) {
                
                if (!self.shouldTimerRun) {
                    DispatchQueue.main.async {
                        self.remainingTime = self.timerService.getTimeText(secondsToEnd: 0)
                    }
                    
                    return
                }
                
                DispatchQueue.main.async {
                    self.remainingTime = self.timerService.getTimeText(secondsToEnd: secondsToEnd)
                }
                
                secondsToEnd -= 1
                sleep(1)
            }
        }
    }
    
    func stop () {
        stopStartButtonName = "Start"
        shouldTimerRun = false
    }
    
    func cancel () {
        shouldTimerRun = false
    }
}
