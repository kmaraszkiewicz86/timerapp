//
//  ContentView.swift
//  TimerApp
//
//  Created by Krzysztof Maraszkiewicz on 10/10/2021.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var timerViewModel: TimerViewModel
    
    var body: some View {
        VStack {
            Text("Remaining time:")
            
            Text("\(timerViewModel.remainingTime)")
            
            Text("Provide some time and click start")
                .padding()
            
            HStack {
                Picker(selection: $timerViewModel.hours, label: Text("Hours")) {
                    ForEach(0..<5, id: \.self) {
                        i in
                        Text("\(i)")
                    }
                }
                
                Picker(selection: $timerViewModel.minutes, label: Text("Minutes")) {
                    ForEach(0..<60, id: \.self) {
                        i in
                        Text("\(i)")
                    }
                }
                
                Picker(selection: $timerViewModel.seconds, label: Text("Seconds")) {
                    ForEach(1..<60, id: \.self) {
                        i in
                        Text("\(i)")
                    }
                }
            }
            
            HStack {
                Button("Cancel") {
                    timerViewModel.cancel()
                }
                
                Button(timerViewModel.stopStartButtonName) {
                    timerViewModel.timerButtonWork()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(timerViewModel: TimerViewModel(TimerService()))
    }
}
