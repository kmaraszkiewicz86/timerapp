//
//  TimerService.swift
//  TimerApp
//
//  Created by Krzysztof Maraszkiewicz on 10/10/2021.
//

import Foundation

class TimerService {
    
    func getTimeText(secondsToEnd: Int) -> String {
        let timeModel = getTimeFromSeconds(secondsToEnd: secondsToEnd)
        return getDescriptionTime(timeModel: timeModel)
    }
    
    func getSecondsFromTime(timerModel: TimeModel) -> Int {
        
        let secondsFromHours = timerModel.hours * 60 * 60
        let secondsFromMinutes = timerModel.minutes * 60
        
        return secondsFromHours + secondsFromMinutes + timerModel.seconds
    }
    
    func getTimeFromSeconds(secondsToEnd: Int) -> TimeModel {
        
        if (secondsToEnd <= 0) {
            return TimeModel(hours: 0, minutes: 0, seconds: 0)
        }
        
        let hours = (secondsToEnd / 60) / 60
        var minutes = 0
        var seconds = 0
        
        var remainingSeconds = secondsToEnd - (hours * 60 * 60)
        
        if (remainingSeconds > 0) {
            minutes = remainingSeconds / 60
        }
        
        remainingSeconds = remainingSeconds - (minutes * 60)
        
        seconds = remainingSeconds
        
        return TimeModel(hours: hours, minutes: minutes, seconds: seconds)
    }
    
    func getDescriptionTime (timeModel : TimeModel) -> String {
        
        let hours = getHourOrMinutesOrSeconds(value: timeModel.hours)
        let minutes = getHourOrMinutesOrSeconds(value: timeModel.minutes)
        let seconds = getHourOrMinutesOrSeconds(value: timeModel.seconds)
        
        return "\(hours):\(minutes):\(seconds)"
    }
    
    private func getHourOrMinutesOrSeconds(value: Int) -> String {
        if (value < 10) {
            return "0\(value)"
        }
        
        return "\(value)"
    }
    
}
