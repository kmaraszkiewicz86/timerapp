//
//  TimerModel.swift
//  TimerApp
//
//  Created by Krzysztof Maraszkiewicz on 10/10/2021.
//

import Foundation

struct TimeModel : Equatable {
    
    var hours : Int
    var minutes : Int
    var seconds : Int
}
